@extends('template.master')

@section('content')
<div class="card m-3">
    <div class="card-header d-flex">
      <h2 class="card-title"><b>{{$pertanyaan->judul}}</b></h2>

    </div>
    <div class="card-body">
        {!!$pertanyaan->isi!!}
    </div>
    <div class="card-footer">
        <div class="d-flex card-footer">
            @guest
            <a href="#" class="btn btn-sm btn-default" disabled><i class="far fa-thumbs-up" ></i></a><span> 200</span>
            <a href="#" class="btn btn-sm btn-default" disabled><i class="far fa-thumbs-down" ></i></a><span> 200</span>
            @else

                <a href="#" class="btn btn-sm btn-default"><i class="far fa-thumbs-up" ></i></a><span> 200</span>
                <a href="#" class="btn btn-sm btn-default"><i class="far fa-thumbs-down" ></i></a><span> 200</span>
                <a href="/komentar-question/{{$pertanyaan->id}}/show" style="color: grey; margin-left:10px" >{{$jumkomen}} komentar</a>
                <a class="btn btn-sm btn-success ml-auto" href="/komentar-question/{{$pertanyaan->id}}">Beri Komentar</a>
            @endguest
        </div>
    </div>
</div>
<div>
    <br>
    <h2 class="ml-3">Komentar Pertanyaan : </h2>
        @if (session('success'))
            <div class="alert alert-success">
                <p>{{session('success')}}</p>
            </div>
        @endif
    <br>
</div>
@forelse ($komen as $item=>$value)
    <div class="card m-3">
        <div class="card-header d-flex">
            <h2 class="card-title"><b>Comment:: {{$pertanyaan->judul}}</b></h2>
            <div class="ml-auto" >
                <a  class="ml-2" href="/komentar-question/{{$value->id}}/edit" style="color: rgb(126, 126, 126)">Edit</a>
            </div>

        </div>
        <div class="card-body">
            {!!$value->isi!!}
        </div>
        <div class="card-footer">
            <div class="d-flex card-footer">
                @guest
                <i class="far fa-thumbs-up" ></i><span> 200</span>
                <i class="far fa-thumbs-down" ></i><span> 200</span>
                @else

                <ul class="ml-auto d-flex" style="list-style-type: none;">
                    <li class="ml-2">
                        <form action="/komentar-question/{{$value->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger ml-2">
                        </form>
                    </li>
                </ul>

                @endguest
            </div>
        </div>
    </div>
@empty
    <div class="callout callout-danger">
        <h5>BELUM ADA KOMENTAR !</h5>
        <p>SILAHKAN TUNGGU KOMENTAR ANDA</p>
    </div>
@endforelse

@endsection
