@extends('template.master')

@section('content')
<div class="card m-3">
    <div class="card-header d-flex">
      <h2 class="card-title"><b>{{$data->judul}}</b></h2>
        <a class="btn btn-sm btn-success ml-auto" href="/komentar-question/{{$data->id}}">Beri Komentar</a>
    </div>
    <div class="card-body">
        {!!$data->isi!!}
    </div>
    <div class="card-footer">
        <div class="d-flex card-footer">
            @guest
            <i class="far fa-thumbs-up" ></i><span> {{$like_pertanyaan}}</span>
            @else

                <a href="/like/{{$data->id}}" class="btn btn-sm btn-default"><i class="far fa-thumbs-up" ></i></a><span> {{$like_pertanyaan}}</span>
                <a href="/komentar-question/{{$data->id}}/show" style="color: grey; margin-left:10px" >{{$komentar}} komentar</a>
                <a class="btn btn-success ml-auto" href="/jawaban/{{$data->id}}">Jawab Pertanyaan</a>
            @endguest
        </div>
    </div>
</div>
<div>
    <br>
    <h2 class="ml-3">Jawaban Pertanyaan : </h2>
    <br>
</div>
@forelse ($answer as $item=>$value)
    <div class="card m-3">
        <div class="card-header d-flex">
            <h2 class="card-title"><b>Re:: {{$data->judul}}</b></h2>
            <div class="ml-auto" >
                <a  class="ml-2" href="/jawaban/{{$value->id}}/edit" style="color: rgb(126, 126, 126)">Edit</a>
            </div>

        </div>
        <div class="card-body">
            {!!$value->isi!!}
        </div>
        <div class="card-footer">
            <div class="d-flex card-footer">
                @guest
                <a href="#" class="btn btn-sm btn-default ml-2" disabled><i class="far fa-thumbs-up" ></i></a><span> 200</span>
                <a href="#" class="btn btn-sm btn-default ml-2" disabled><i class="far fa-thumbs-down" ></i></a><span> 200</span>
                @else

                <ul class="d-flex" style="list-style-type: none;">
                    <li><a href="#" class="btn btn-sm btn-default"><i class="far fa-thumbs-up" ></i></a><span> 200</span></li>
                    <li><a href="#" class="btn btn-sm btn-default"><i class="far fa-thumbs-down" ></i></a><span> 200</span></li>
                </ul>

                <ul class="ml-auto d-flex" style="list-style-type: none;">
                    <li class="ml-2">
                        <form action="/jawaban/{{$value->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger ml-2">
                        </form>
                    </li>
                    <li class="ml-2">
                        <a class="btn btn-success " href="#">Beri Komentar</a>
                    </li>
                </ul>

                @endguest
            </div>
        </div>
    </div>
@empty
    <div class="callout callout-danger">
        <h5>BELUM ADA JAWABAN !</h5>
        <p>SILAHKAN TUNGGU JAWABAN ANDA</p>
    </div>
@endforelse

@endsection
