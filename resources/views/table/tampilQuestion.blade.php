@extends('template.master')
@section('content')

<div class="card m-3">
    <div class="card-header">
      <h2 class="card-title"><b> Daftar Pertanyaan </b></h2>

      {{-- <div class="card-tools">
        <ul class="pagination pagination-sm float-right">
          <li class="page-item"><a class="page-link" href="#">«</a></li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
      </div> --}}
    </div>
    <!-- /.card-header -->
    <div class="card-body"><a class="btn btn-primary" href="{{route('question.create')}}">Tambah Pertanyaan</a></div>
    <div class="card-body p-0">

        @if (session('success'))
            <div class="alert alert-success">
                <p>{{session('success')}}</p>
            </div>
        @endif
      <table class="table">
            @forelse ($data as $item => $dat)
            <tr>
                <td><h4><u><a href="{{route('question.show', ['question' => $dat->id])}}" style="color: black">{{ $dat->judul }} </a></u></h4>
                <span>{!! $dat->isi !!}</span>
                </td>
                <td style="width: 75%;">

                </td>
                <td style="display : flex;">
                    <a href="{{route('question.edit', ['question' => $dat->id])}}" class="btn btn-default ml-2"> Edit </a>

                    <form action="{{route('question.destroy', ['question' => $dat->id])}}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="delete" class="btn btn-danger ml-2">
                    </form>
                </td>

            </tr>
            @empty
            <div class="callout callout-danger">
                <h5>TIDAK ADA PERTANYAAN !</h5>
                <p>SILAHKAN TAMBAHKAN PERTANYAAN TERLEBIH DAHULU</p>
            </div>
            @endforelse

      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection
