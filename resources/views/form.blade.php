<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIGN UP</title>
</head>
<body>
    <h1>BUAT ACCOUNT BARU!</h1>
    <!--Bagian form (dalam action diisi aksi yang akan dilakukan jika submit di klik)-->
    <form action="/welcome" method="POST">
        @csrf
        <!--Bagian form text biasa-->
        <label for="fname">First Name</label><br><br>
        <input type="text" id="fname" name="fname"><br><br>

        <label for="lname">Last Name</label><br><br>
        <input type="text" id="lname" name="lname"><br><br>

        <!--Bagian form radiobutton-->
        <label>Gender :</label><br><br>
        <input type="radio" name="gender" value="0">Male <br>
        <input type="radio" name="gender" value="1">Female <br>
        <input type="radio" name="gender" value="2">Other <br><br>

        <!--Bagian form select-->
        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value="0">Indonesia</option>
            <option value="1">Singapura</option>
            <option value="2">Malaysia</option>
            <option value="3">Australia</option>
        </select><br><br>

        <!--Bagian form cekbox-->
        <label>Language Spoken :</label><br><br>
        <input type="checkbox" name="language" value="0">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="1">English <br>
        <input type="checkbox" name="language" value="2">Other <br><br>

        <!--Bagian form textarea-->
        <label for="bio">Bio :</label><br><br>
        <textarea id="bio" cols="30" rows="10"></textarea>
        <br><br>

        <!--Bagian form submit-->
        <input type="submit" value="Sign Up">&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="/">&larr;Kembali</a>
    </form>
</body>
</html>
