<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $guarded = [];
    public function users(){
        return $this->belongsTo('App\User','users_id');
    }
    public $timestamps = false;
}
