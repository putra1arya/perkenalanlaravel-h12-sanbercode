<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pertanyaanModel extends Model
{
    protected $table ="pertanyaan";
    const CREATED_AT ="tanggal_dibuat";
    const UPDATED_AT ="tanggal_diupdate";
    protected $fillable = ['judul','isi'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
    public function jawaban(){
        return $this->hasMany('App\Jawaban','pertanyaan_id');
    }
    public function questionKomen(){
        return $this->hasMany('App\QuestionKomen', 'pertanyaan_id');
    }
    public function like_user(){
        return $this->belongsToMany('App\User','like_dislike_pertanyaan', 'pertanyaan_id','user_id');
    }
}
