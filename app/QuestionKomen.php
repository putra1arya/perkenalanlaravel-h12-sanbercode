<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionKomen extends Model
{
    protected $table ="komentar_pertanyaan";
    const CREATED_AT ="tanggal_dibuat";
    const UPDATED_AT ="tanggal_diupdate";
    protected $fillable = ['isi'];

    public function pertanyaan(){
        return $this->belongsTo('App\pertanyaanModel','pertanyaan_id');
    }
}
