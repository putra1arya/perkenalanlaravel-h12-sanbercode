<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table ="jawaban";
    const CREATED_AT ="tanggal_dibuat";
    const UPDATED_AT ="tanggal_diupdate";
    protected $fillable = ['isi','user_id'];

    public function pertanyaan(){
        return $this->belongsTo('App\pertanyaanModel','pertanyaan_id');
    }
}
