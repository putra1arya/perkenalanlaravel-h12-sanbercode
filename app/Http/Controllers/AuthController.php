<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome(Request $request){
        // dd($request->all());
        $data = [ 'namaDepan' => $request['fname'],
                    'namaBelakang' => $request['lname']];
        return view('newMember', $data);
    }
}
