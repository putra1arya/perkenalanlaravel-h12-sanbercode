<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\pertanyaanModel;
use Illuminate\Support\Facades\Auth;
use App\User;

class questionformController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    public function create(){
        return view('form.questionForm');
    }
    public function store(Request $request){
        // $tgl = date('Y-m-d H:i:s');
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);
        // $query = DB::table('pertanyaan')->insert([
        //     'judul' => $request['judul'],
        //     'isi' =>$request['isi'],
        //     'tanggal_dibuat' => $tgl
        // ]);

        //single-insert
        // $pertanyaan = new pertanyaanModel;
        // $pertanyaan->judul = $request->judul;
        // $pertanyaan->save();

        //mass-insert
        $tambah = pertanyaanModel::create([
            "judul" =>$request['judul'],
            "isi" => $request['isi'],

        ]);

        $user = Auth::user();

        $user->pertanyaan()->save($tambah);

        return redirect(route("question.index"))->with('success', "Pertanyaan Berhasil Disimpan!");
    }
    public function index(){
        //query builder
        // $data = DB::table('pertanyaan')->get();

        //pake model
        $data = pertanyaanModel::all();
        return view('table.tampilQuestion',compact('data'));
    }
    public function show($id){
        //query builder
        // $data = DB::table('pertanyaan')->where('id',$id)->first();
        //model
        $data = pertanyaanModel::find($id);
        $answer = $data->jawaban;
        $pertanyaan = pertanyaanModel::find($id);
        $like_pertanyaan = $pertanyaan->like_user()->count();
        $komentar = $data->questionKomen->count();
        return view('detail.detailQuestion',compact('data','answer','komentar','like_pertanyaan'));
    }
    public function edit($id){
        //query builder
        // $data = DB::table('pertanyaan')->where('id',$id)->first();
        //model
        $data = pertanyaanModel::where('id',$id)->first();
        return view('form.editQuestion',compact('data'));
    }
    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        //querybuilder

        // $tgl = date('Y-m-d H:i:s');
        // $query = DB::table('pertanyaan')
        //         ->where('id',$id)
        //         ->update([
        //             'judul' => $request['judul'],
        //             'isi' =>$request['isi'],
        //             'tanggal_diupdate' => $tgl
        //         ]);

        //model
        $update = pertanyaanModel::where('id',$id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);
        return redirect('/question')->with('success', 'Berhasil diupdate');
    }
    public function destroy($id){
        //query builder
        // $query = DB::table('pertanyaan')->where('id',$id)->delete();

        //model
        $query = pertanyaanModel::destroy($id);
        return redirect('/question')->with('success', 'Berhasil dihapus');
    }
    public function like($id){
        $user_id = Auth::id();
        $pertanyaan = pertanyaanModel::find($id);
        $pertanyaan->like_user()->toggle($user_id);
        return redirect('/question/'.$id);
    }
}
