<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pertanyaanModel;
use App\QuestionKomen;
use Auth;

class quesKomController extends Controller
{
    public function create($id){
        $query = pertanyaanModel::find($id);
        return view('komentar.quesKomenForm', compact('query'));
    }
    public function store(Request $request, $id){
        $request->validate([
            'isi' => 'required'
        ]);
        $komen = QuestionKomen::create([
            'isi' => $request['isi'],
            'user_id' => Auth::id()
        ]);
        $pertanyaan = pertanyaanModel::find($id);
        $pertanyaan->questionKomen()->save($komen);
        return redirect('/question/'.$id)->with('success', 'Berhasil Menambahkan Komentar');
    }
    public function show($id){
        $pertanyaan = pertanyaanModel::find($id);
        $komen = $pertanyaan->questionKomen;
        $jumkomen = $komen->count();
        return view('komentar.quesViewKomen', compact('pertanyaan','komen','jumkomen'));
    }
    public function edit($id){
        $komen = QuestionKomen::find($id);
        $question = pertanyaanModel::find($komen->pertanyaan_id);
        return view('komentar.quesEditKomen',compact('komen','question'));
    }
    public function update(Request $request,$pertanyaan, $komen){
        $query = QuestionKomen::where('id',$komen)->update([
            'isi' => $request['isi']
        ]);
        return redirect('/komentar-question/'.$pertanyaan.'/show')->with('success', 'Berhasil Mengedit Komentar');
    }

    public function destroy($id){
        $pertanyaan = QuestionKomen::find($id);
        $query = QuestionKomen::destroy($id);
        return redirect('/komentar-question/'.$pertanyaan->pertanyaan_id.'/show');
    }
}
