<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jawaban;
use Auth;
use App\pertanyaanModel;

class jawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data = pertanyaanModel::find($id);
        return view('jawaban.formJawaban',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request->validate([
            'isi' => 'required'
        ]);

        $jawaban = new \App\Jawaban([
            'isi'=> $request['isi'],
            'user_id' => Auth::id()
        ]);

        $pertanyan = \App\pertanyaanModel::find($id);
        $pertanyan->jawaban()->save($jawaban);
        // return redirect('/question/'. $id);
        return redirect()->route('question.show', ["question" => $id])->with('success','Berhasil Memberikan Jawaban');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($jawaban)
    {
        $answer = Jawaban::find($jawaban);
        $question = pertanyaanModel::find($answer->pertanyaan_id);
        return view('jawaban.editJawaban',compact('answer','question'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $pertanyan, $id)
    {
        $request->validate([
            'isi' => 'required'
        ]);
        $query = Jawaban::where('id',$id)->update([
            'isi' => $request['isi']
        ]);
        return redirect('/question/'.$pertanyan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan_id = Jawaban::find($id);
        $query = Jawaban::destroy($id);
        return redirect('/question/'.$pertanyaan_id->pertanyaan_id);
    }
}
