<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\questionformController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('table.table');
});

Route::get('/data-tables', function() {
    return view('table.tbData');
});

// Route::get('/question/form-question','questionformController@tampilform');
// Route::post('/question','questionformController@store');
// Route::get('/question','questionformController@tampil');
// Route::get('/question/{id}/edit', 'questionformController@edit');
// Route::get('/question/{id}', 'questionformController@detail');
// Route::put('/question/{id}', 'questionformController@update');
// Route::delete('/question/{id}', 'questionformController@destroy');

//pertanyaan
Route::resource('question', 'questionformController');

//jawaban
Route::get('/jawaban/{id}', 'jawabanController@create');
Route::post('/jawaban/{id}', 'jawabanController@store');
Route::get('/jawaban/{jawaban}/edit', 'jawabanController@edit');
Route::put('/jawaban/{pertanyaan}/{jawaban}', 'jawabanController@update');
Route::delete('/jawaban/{id}', 'jawabanController@destroy');

//komentar pertanyaan
Route::get('/komentar-question/{id}', 'quesKomController@create');
Route::post('/komentar-question/{id}', 'quesKomController@store');
Route::get('/komentar-question/{id}/show', 'quesKomController@show');
Route::get('/komentar-question/{id}/edit', 'quesKomController@edit');
Route::put('/komentar-question/{pertanyaan}/{komentar}', 'quesKomController@update');
Route::delete('/komentar-question/{id}','quesKomController@destroy');
Auth::routes();

//like
Route::get('/like/{id}', 'questionformController@like');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register','HomeController@formurl');
Route::post('/welcome', 'AuthController@welcome');
